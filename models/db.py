# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
#########################################################################

if not request.is_https and request.env.remote_addr != "127.0.0.1":
    redirect('https://delia.app.vanderbilt.edu'+request.env.web2py_original_uri)

if KV_DBASE == "mysql":
    db = DAL('mysql://%s:%s@%s/%s' % (KV_DBASE_USERNAME,KV_DBASE_PASSWORD,KV_DBASE_HOST,KV_DBASE_NAME))
    #db = DAL('mysql://vote:b411()t@localhost/vote')       # if not, use SQLite or other DB
    session.connect(request, response, masterapp="vote")
else:
    db = DAL('sqlite://storage.sqlite')
    session.connect(request, response, masterapp="vote")

# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
#response.generic_patterns = ['*'] if request.is_local else []

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

#This prevents error messages from appearing when the database is changed.
#If you are experiencing database problems, try setting migratevar manually.
if KV_DBASE_MIGRATE:
    migratevar = True
else:
    migratevar = False

from gluon.tools import Mail, Auth, Crud, Service, PluginManager, prettydate
mail = Mail()                                  # mailer
auth = Auth(globals(),db)                                # authentication/authorization
crud = Crud(globals(),db)                                # for CRUD helpers using auth
service = Service(globals())                            # for json, xml, jsonrpc, xmlrpc, amfrpc
plugins = PluginManager()                      # for configuring plugins

#This code handles password recovery emails.
#It didn't work so I removed the links from the front page.
mail.settings.server = 'smtp.gmail.com:587'  # your SMTP server 'logging' or 
mail.settings.sender = 'info.filtergraph@gmail.com'         # your email
mail.settings.login = 'info.filtergraph:*()+345rph'      # your credentials or None

#commented out because it didn't work in old server
#auth.settings.hmac_key = 'sha512:2f843a12-6ef3-45c2-ab0d-9d40b208dc87'   # before define_tables()

#define auth tables (auth_user, auth_event, etc)
auth.define_tables(username=True,migrate=migratevar)                           # creates all needed tables

#auth settings
auth.settings.mailer = mail                    # for user email verification
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False

#disables default user registration
auth.settings.actions_disabled = ('register')

#more auth_settings
auth.messages.verify_email = 'Click on the link http://'+request.env.http_host+URL('default','user',args=['verify_email'])+'/%(key)s to verify your email'
auth.settings.reset_password_requires_verification = True
auth.messages.reset_password = 'A password reset for KELT North & South Candidate Selection has been requested for this e-mail.\n\nClick this link to reset your password: http://'+request.env.http_host+URL('default','user',args=['reset_password'])+'/%(key)s\n\nIf you did not request a password reset, you may disregard this message.\n\nThanks,\nThe KELT North & South Candidate Selection Team'
auth.settings.login_next = session.home
auth.settings.logout_next = session.home
auth.settings.profile_next = session.home
auth.settings.register_next = session.home
auth.settings.retrieve_username_next = session.home
auth.settings.retrieve_password_next = session.home
auth.settings.change_password_next = URL("user",args=["profile"])
auth.settings.request_reset_password_next = session.home
auth.settings.reset_password_next = session.home
auth.settings.verify_email_next = session.home

#expiration of logins
auth.settings.expiration = 2592100  # seconds
auth.settings.long_expiration = 2592100  # seconds
auth.settings.remember_me_form = False

#########################################################################
## If you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, uncomment and customize following
# from gluon.contrib.login_methods.rpx_account import RPXAccount
# auth.settings.actions_disabled = \
#    ['register','change_password','request_reset_password']
# auth.settings.login_form = RPXAccount(request, api_key='...',domain='...',
#    url = "http://localhost:8000/%s/default/user/login" % request.application)
## other login methods are in gluon/contrib/login_methods
#########################################################################

#we don't use crud, so ignore
crud.settings.auth = None        # =auth to enforce authorization on crud

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

#stores all votes
db.define_table('votes',
    Field('question','string',notnull=True,label="Question"),
    Field('answer','string',label="Answer"),
    Field('comment','string',label="Comment"),
    Field('voter',db.auth_user,default=auth.user_id,writable=False,readable=False,label="Voter"),
    Field('active','boolean',default=True,label="Active"),
    Field('time','datetime',default=request.now,writable=False,readable=False,label="Time of vote"),
    Field('telescope','string',label="Telescope")
    ,migrate=migratevar)

#not used, kept for archival purposes
db.define_table('feedback',
    Field('comment','text',label="Comment"),
    Field('name','string',label="Name"),
    Field('time','datetime',default=request.now,writable=False,readable=False,label="Time submitted")
    ,migrate=migratevar)
    
#stores elections
db.define_table('elections',
    Field('id','id',
          represent=lambda id:SPAN(id,' ',A('view',_href=URL('elections_read',args=id)))),
    Field('name','string',label="Name"),
    Field('open_time','datetime',default=request.now,label="Open time"),
    Field('close_time','datetime',default=request.now,label="Close time")
    ,migrate=migratevar)

#stores expiry times
db.define_table('expiry_times',
    Field('id','id',
          represent=lambda id:SPAN(id,' ',A('view',_href=URL('expiry_times_update',args=id)))),
    Field('name','string',label="Name"),
    Field('expiry_time','datetime',default=request.now,label="Expiry time"))