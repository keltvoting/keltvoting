# -*- coding: utf-8 -*-

import os, ConfigParser
config = ConfigParser.RawConfigParser()
config.read(os.path.join(request.env.gluon_parent,request.application+".cfg"))

#http://stackoverflow.com/questions/4028904/how-to-get-the-home-directory-in-python

KV_DBASE = config.get("database","database")
if KV_DBASE != "sqlite":
    KV_DBASE_USERNAME = config.get("database","username")
    KV_DBASE_PASSWORD = config.get("database","password")
    KV_DBASE_HOST = config.get("database","host")
    KV_DBASE_NAME = config.get("database","name")
try:
    KV_DBASE_MIGRATE = (config.get("database","migrate") == "true")
except:
    KV_DBASE_MIGRATE = True