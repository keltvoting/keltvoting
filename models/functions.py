import datetime

#This function determines whether or not a poll is open
#question: the name of the candidate or field
#field: "north", "south" or "joint"
#novotetest: if user has no voting privileges, always returns false
#returns: close time of election if open, False if closed
def polls_open(question,field,novotetest=True):
    #do not open polls if user is view only
    if auth.has_membership('no-vote') and novotetest:
        return False
    #get elections from database
    print "DB Select: get elections from database"
    elections = db(db.elections.id>0).select()
    for election in elections:
        #KELT North
        if election.name.startswith("north-") and field == "north" and election.name[6:] in question and election.open_time < request.now and request.now < election.close_time:
            return election.close_time
        #KELT South
        elif election.name.startswith("south-") and field == "south" and election.name[6:] in question and election.open_time < request.now and request.now < election.close_time:
            return election.close_time
        #KELT Joint
        elif election.name.startswith("joint-") and field == "joint" and election.name[6:] in question and election.open_time < request.now and request.now < election.close_time:
            return election.close_time
        #old KELT elections (opens all fields with the same number)
        elif election.name in question and election.open_time < request.now and request.now < election.close_time:
            return election.close_time
    #no election found, return false
    return False

#Get the expiry time of a poll
#question: the name of the candidate or field
#field: "north", "south" or "joint"
#returns: expiry time of election
def expiry_time(question,field):
    #get expiry time from database
    print "DB Select: get expiry time from database"
    exptimes = db(db.expiry_times.id>0).select(orderby=~db.expiry_times.expiry_time)
    for exptime in exptimes:
        #KELT North
        if exptime.name.startswith("north-") and field == "north" and exptime.name[6:] in question:
            return exptime.expiry_time
        #KELT South
        elif exptime.name.startswith("south-") and field == "south" and exptime.name[6:] in question:
            return exptime.expiry_time
        #KELT Joint
        elif exptime.name.startswith("joint-") and field == "joint" and exptime.name[6:] in question:
            return exptime.expiry_time
        #old KELT exptimes (opens all fields with the same number)
        elif exptime.name in question:
            return exptime.expiry_time
    #no exptime found, return Dec 17, 1987
    return datetime(1987,12,17)