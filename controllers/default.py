# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

import os, re, zlib
from datetime import datetime
from gluon.tools import prettydate

#old code
#Retrieves user info bar on top of screen
def user_info():
    return dict()

#Old routine for submitting votes. Do not comment out. Note that submit_vote_quick calls submit_vote but returns a different view.
def submit_vote():
    #Check if user is blocked
    if auth.has_membership('blocked'):
        return "<script type='text/javascript'>alert('Your account has been deactivated.')</script>"
    question = request.vars.q
    field = request.vars.f
    #Alert user if voting has closed
    if not polls_open(question,field):
        return "<script type='text/javascript'>alert('Voting has been closed.')</script>"
    #Alert user if not logged in
    if not auth.user_id:
        return "<script type='text/javascript'>alert('You are not logged in. Please login to cast your vote.')</script>"
    answer = request.vars.a
    comment = request.vars.c or ""
    #If text is "Add a comment..." then do not record the comment
    if comment == "Add a comment...":
        comment = ""
    #Deactivate old votes where the candidate name, field and user match
    print "DB Update: Deactivate old votes where the candidate name, field and user match"
    db(db.votes.question==question)(db.votes.telescope==field)(db.votes.voter==auth.user_id).update(active=False)
    #Record the new vote
    print "DB Insert: Record the new vote"
    db.votes.insert(question=question, answer=answer, comment=comment,telescope=field)
    #In Javascript, let the user know that the vote has been cast
    return dict(question=question,answer=answer,comment=comment)
    
#New view for submitting votes. This calls the routine above but returns a different view.
def submit_vote_quick():
    return submit_vote()

@auth.requires_login()
def review_votes():
    #Deny access to blocked users
    if auth.has_membership('blocked'):
        return response.render('default/user_info.html') + "<br/><br/><h1>Your account has been deactivated</h1>According to our records you are no longer a member of the KELT project and we have deactivated your account.<br/><br/>If you believe you have received this message in error, contact <a href='http://jpepper.cas2.lehigh.edu/'>Joshua Pepper</a>.<br/><br/>Click <a href='/"+request.application+"/default/user/logout'>here</a> to logout."
    if request.vars.inactive_votes:
        #Get all information
        print "DB Select: Get all information"
        votes = db(db.votes.voter==auth.user_id).select()
    else:
        #Get selected information from active votes only
        print "DB Select: Get selected information from active votes only"
        votes = db(db.votes.voter==auth.user_id)(db.votes.active==True).select(db.votes.question,db.votes.answer,db.votes.comment,db.votes.time,db.votes.telescope)
    #Return information to user
    return dict(votes=votes)
    
#Old feedback form
"""def feedback():
    form = SQLFORM(db.feedback)
    if auth.user_id:
        try:
            form.vars.name = auth.user.first_name + " " + auth.user.last_name
        except:pass
    if form.accepts(request.vars, session):
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)"""
    
#User handling routines: changing passwords, logging in, logging out...
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    if not auth.user_id:
        response.menu = []
    if auth.has_membership("guest") and request.args[0] != 'logout':
        return "This function is disabled."
    else:
        return dict(form=auth())

#Not used - yet
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)

#Not used - yet
def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
    
#Quick loading
@auth.requires_login()
def quick():
    #Deny access to blocked users
    if auth.has_membership('blocked'):
        return response.render('default/user_info.html') + "<br/><br/><h1>Your account has been deactivated</h1>According to our records you are no longer a member of the KELT project and we have deactivated your account.<br/><br/>If you believe you have received this message in error, contact <a href='http://jpepper.cas2.lehigh.edu/'>Joshua Pepper</a>.<br/><br/>Click <a href='/"+request.application+"/default/user/logout'>here</a> to logout."
    #Do not accept ".." to prevent hacking
    if ".." in request.args:
        return "Nope"
    #Get url starting with "quick"
    fname = request.url
    fname = fname[fname.index("quick"):]
    #Remove '/' at end of URL if needed
    if fname[-1] == "/":
        redirect('/'+fname[:-1])
    #If URL ends with '+' then return filename (debug mode)
    debug = False
    if fname.endswith("+"):
        debug = True
        fname = fname[:-1]
    #Filename rules
    #First column = regular expression or exact match
    #Second column = string to look for
    #Third column = string to replace
    fname_rules = [
        ("re","(\d+)/figs_\\1", "figs_\\1"),
        ("re","(\d+)/old_figs_\\1", "old_figs_\\1"),
        ("re","^quick/north/?$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kn_cand_selection/all_index.html"),
        ("re","^quick/south/?$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/ks_cand_selection/all_index.html"),
        ("re","^quick/joint/?$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kj_cand_selection/all_index.html"),
        ("re","^quick/north/(\d+)$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kn_cand_selection/index_\\1.html"),
        ("re","^quick/north/","/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kn_cand_selection/"),
        ("re","^quick/south/(\d+)$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/ks_cand_selection/index_\\1.html"),
        ("re","^quick/south/","/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/ks_cand_selection/"),
        ("re","^quick/joint/(\d+)$", "/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kj_cand_selection/index_\\1.html"),
        ("re","^quick/joint/","/net/vieja/export/vieja/scr2012/scr3/kelt_scratch/kj_cand_selection/"),
    ]
    #Apply filename rules
    for r in fname_rules:
        if r[0] == "re":
            fname = re.sub(r[1],r[2],fname)
        else:
            fname = fname.replace(r[1],r[2])
    #If fname is not found, use fname2
    fname2 = fname.replace("selection/","selection/old_")
    use_fname2 = False
    #Return filename in debug mode
    if debug or request.vars.debug:
        return fname
    #Turn on keep alive
    response.headers['Connection']="Keep-Alive"
    response.headers['Keep-Alive']="timeout=20, max=100"
    #Stream PNG files
    if fname.endswith("png"):
        #https://en.wikipedia.org/wiki/Mime_type
        #http://web2py.com/examples/default/examples
        response.headers['Content-Type']="image/png"
        response.headers['Cache-Control'] = 'max-age=315360000'
        response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
        response.headers['Pragma'] = ''
        try:
            return response.stream(open(fname,'rb'),chunk_size=4096)
        except:pass
        try:
            return response.stream(open(fname2,'rb'),chunk_size=4096)
            use_fname2 = True
        except:pass
        response.headers['Content-Type']="text/ascii"
        response.headers['Content-Disposition']=None
        return fname
    #Stream JPG files
    if fname.endswith("jpg"):
        #https://en.wikipedia.org/wiki/Mime_type
        #http://web2py.com/examples/default/examples
        response.headers['Content-Type']="image/jpg"
        response.headers['Cache-Control'] = 'max-age=315360000'
        response.headers['Expires'] = 'Thu, 31 Dec 2037 23:59:59 GMT'
        response.headers['Pragma'] = ''
        try:
            return response.stream(open(fname,'rb'),chunk_size=4096)
        except:pass
        try:
            return response.stream(open(fname2,'rb'),chunk_size=4096)
            use_fname2 = True
        except:pass
        response.headers['Content-Type']="text/ascii"
        response.headers['Content-Disposition']=None
        return fname
    #Load file
    try:
        f = open(fname,'r')
        page = f.read()
        f.close()
    except:
        try:
            f = open(fname2,'r')
            page = f.read()
            f.close()
            fname = fname2
            use_fname2 = True
        except:
            #return 404 error
            raise HTTP(404)
    #Page rules
    #First column = URL exact match
    #Second column = filename exact match
    #Third column = regular expression or exact match
    #Fourth column = string to look for
    #Fifth column = string to replace
    page_rules = [
        ('','index','re','<script.*voting_table_v4.js.*>(</script>)?', ""), #remove old js
        ('','','re','<script.*voting_superwasp.*>(</script>)?', ''), #remove old js
        ('','','re','<script.*voting_variability.*>(</script>)?', ''), #remove old js
        ('','','re','<script.*voting_info_page.*>(</script>)?', ''), #remove old js
        ('','A','exact','jquery-1.5.min.js','../jquery-1.5.min.js'),
        ('','V','exact','jquery-1.5.min.js','../jquery-1.5.min.js'),
        ('','S','exact','jquery-1.5.min.js','../jquery-1.5.min.js'),
        ('','K','exact','jquery-1.5.min.js','../jquery-1.5.min.js'),
        ('','','exact','<head>','<head><link rel="shortcut icon" href="/votedev/static/favicon.ico" type="image/x-icon">'),
        ('','index','exact','<script type="text/javascript" id="js">','<script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script><script type="text/javascript" src="/vote/static/js/jquery.lazyload.min.js"></script><script type="text/javascript" id="js">'), #lazyload
        ('','index','exact','img src="figs','img class="lazy" style="width:600px;height:300px" src="/vote/static/images/gray.png" data-original="figs'), #lazyload
        ('','index','exact','tablesorter();','tablesorter();$("img.lazy").lazyload({ effect : "fadeIn", threshold:500});$("#myTable").bind("sortEnd",function() {$("img.lazy").lazyload({ effect : "fadeIn", threshold:500});});'), #lazyload
        ('','','re','<body([^>]*)>', '<body\\1>##USERINFO##<div style="height:24px;"></div><br/><br/>'), #userinfo
        ('','','re','pagehelp.html','/quick/north/pagehelp.html'), #help
        ('','','re','indexhelp.html','"/quick/north/indexhelp.html"'), #help
        ('north/all_index.html','','re','index_(\d\d).html', '\\1'),
        ('south/all_index.html','','re','index_(\d\d).html', '\\1'),
        ('joint/all_index.html','','re','index_(\d\d).html', '\\1'),
        ('north','all_index','re','index_(\d\d).html', 'north/\\1'),
        ('south','all_index','re','index_(\d\d).html', 'south/\\1'),
        ('joint','all_index','re','index_(\d\d).html', 'joint/\\1'),
        ('','all_index','exact','</title>', '</title><script type="text/javascript" src="/vote/default/quick/north/jquery-1.5.min.js"></script><script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script>'),
        ('','help.html','exact','</title>', '</title><script type="text/javascript" src="/vote/default/quick/north/jquery-1.5.min.js"></script><script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script>'),
        ('','index_','re','index_(\d\d).html','\\1'),
        ('','K','re','index_(\d\d).html','\\1'),
        ('','V','re','index_(\d\d).html','../\\1'),
        ('','A','re','index_(\d\d).html','../\\1'),
        ('','S','re','index_(\d\d).html','../\\1'),
        ('','index','exact','<td style="vertical-align: middle; background-color: C6DEFF; text-align: center;">            </td>','<td style="vertical-align: middle; background-color: C6DEFF; text-align: left;">##WIDGET##</td>'),
        ('','K','exact',"></td><tr>","></td><td rowspan='2' style='background:rgb(198, 222, 255)'>##WIDGET##</td></tr><tr>"),
        ('','V','exact','<span style="font-weight: bold"> Undetrended','<div style="background:rgb(198, 222, 255)">##WIDGET##</div><br/><span style="font-weight: bold"> Undetrended'),
        ('','A','exact','<table>','<div style="background:rgb(198, 222, 255)">##WIDGET##</div><br/><table>'),
        ('','S','exact','<table>','<div style="background:rgb(198, 222, 255)">##WIDGET##</div><br/><table>'),
        ('','','exact','</center></td>\n<td rowspan="1"> <center>',''),
        ('','','exact','</center></td>\n</tr>\n<td rowspan="1"> <center>',''),
        ('','all_index','re','Field (\d\d)','Field \\1##FIELDWIDGET##'),
        ('','','exact','simbak.cfa.harvard.edu/simbad','simbad.u-strasbg.fr/simbad'),
        ('','K','exact','<script type="text/javascript">','<script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script><script type="text/javascript">'), #mousetrap
        ('','V','exact','<script type="text/javascript">','<script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script><script type="text/javascript">'), #mousetrap
        ('','A','exact','<script type="text/javascript">','<script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script><script type="text/javascript">'), #mousetrap
        ('','S','exact','<script type="text/javascript">','<script type="text/javascript" src="/vote/static/js/mousetrap.min.js"></script><script type="text/javascript">'), #mousetrap
    ]
    #Apply page rules
    for r in page_rules:
        if r[0] in request.url and r[1] in fname:
            if r[2] == "re":
                page = re.sub(r[3],r[4],page)
            else:
                page = page.replace(r[3],r[4])
    #Use to disable a page if necessary
    #if fname.endswith("index_06.html") and request.application == "vote":
    #    disabled = True
    #    page = "##USERINFO##<br/><br/>This page is temporarily down for maintenance. We hope to have it ready soon."
    #else:
    disabled = False
    #Store ascii results
    ascii_results = []
    cachetext = ""
    
    #for candidate, variability and SuperWASP pages
    widget_from_cache = False
        
    #Assemble index pages for north, south and joint
    if ('all' in fname):
        #Add open status for each field.
        #The prettydate function turns the closing time into words ("3 days from now", etc.)
        fields = re.findall("Field (\d+)",page)
        for f in fields:
            closetime = polls_open("K"+f,request.args[0],novotetest=False)
            if closetime:
                page = re.sub('##FIELDWIDGET##'," (open until %s)" % prettydate(closetime,T),page,1)
            else:
                page = re.sub('##FIELDWIDGET##',"",page,1)
        
    if ('htm' in fname) and ('all' not in fname) and ('help' not in fname) and not disabled:
        #start_time = datetime.now()
        #Retrieve candidates. Note that any letters between "K" and the field number are omitted.
        #As a result K01C12345 and KC01C12345 are synonymous with each other
        candidates = ["K"+c[0]+"C"+c[1] for c in re.findall("K\D*(\d+)C(\d+)",page)]
        #http://stackoverflow.com/questions/7961363/python-removing-duplicates-in-lists
        #Remove duplicate candidates
        for i in range(1,len(candidates)):
            if i >= len(candidates):
                break
            if candidates[i-1] == candidates[i]:
                candidates.pop(i)
            if i >= len(candidates):
                break
        #Check if polls are open
        popen = polls_open(candidates[0],request.args[0])
        exptime = expiry_time(candidates[0],request.args[0])
        expvotes = 0
        #Check if cache exists
        cachename = "/hd1/"+request.application+"-cache/"+request.args[0]+"-"+fname.split("/")[-1]
        asciiname = "/scratch/viejascr2012/scr3/kelt_scratch/voting_results/"+request.args[0]+"-"+fname.split("/")[-1] + ".txt"
        if os.path.exists(cachename) and not popen and not request.vars.tabulate:
            #Read cache
            with open(cachename) as f:
                page = f.read()
            if auth.has_membership('admin'):
                cachetext += "<br/><br/><div style='background: white;text-align: left;padding: 4px;border-bottom: 1px #B0B0B0 solid;font-family: \"Trebuchet MS\", Helvetica, sans-serif;'>This page is loading from cache. <a href='?tabulate=true'>Reload page</a><br/>Only admins (Dan, Keivan, Josh, Rob, Joey) can see this text.</div>"
        else:
            if popen:
                #Voting open
                if 'index' in fname:
                    #Get all needed entries from the database and store them in the bank
                    print "DB Select: Get all needed entries from the database and store them in the bank"
                    bank = db(db.votes.question.startswith("K"+c[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True')(db.votes.voter==auth.user_id).select()
                else:
                    #We only need the first candidate
                    candidates = candidates[0:1]
                    #Get the candidate entry and store it in the bank
                    print "DB Select: Get the candidate entry and store it in the bank"
                    bank = db(db.votes.question.startswith(candidates[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True')(db.votes.voter==auth.user_id).select()
                #Delete any old cache files
                index_cachename = "/hd1/"+request.application+"-cache/"+request.args[0]+"-index_"+c[0]+".html"
                index_asciiname = "/scratch/viejascr2012/scr3/kelt_scratch/voting_results/"+request.args[0]+"-index_"+c[0]+".html.txt"
                #return index_asciiname
                if os.path.exists(index_cachename):
                    os.remove(index_cachename)
                if os.path.exists(index_asciiname):
                    os.remove(index_asciiname)
            else:
                #Voting closed + no cache exists
                if 'index' in fname:
                    #Get all entries for that field. We will be caching later
                    print "DB Select: Get all entries for that field. We will be caching later"
                    bank = db(db.votes.question.startswith("K"+c[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True').select()
                else:
                    #We only need one candidate
                    candidates = candidates[0:1]
                    #Get information for that candidate
                    widget_name = "/hd1/"+request.application+"-cache/widgets/"+request.args[0]+'-'+candidates[0]
                    if os.path.exists(widget_name):
                        with open(widget_name) as f:
                            bb = f.read()
                        widget_from_cache = True
                    else:
                        print "DB Select: Get information for that candidate"
                        bank = db(db.votes.question.startswith(candidates[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True').select()
            #return repr(datetime.now() - start_time)
            for c in candidates:
                if not widget_from_cache:
                    #Get votes for that candidate
                    votes = bank.exclude(lambda vote: vote.question.startswith(c) and vote.active==True)
                    #Get curent response and comment for the candidate
                    current_vote = [(vote.answer,vote.comment,vote.time) for vote in votes if vote.question==c and vote.voter==auth.user_id]
                    if len(current_vote)>0:
                        current_vote = current_vote[0]
                    else:
                        current_vote = ("","",None)
                    if current_vote[2]:
                        current_vote_ts = "Last edit was %s" % prettydate(current_vote[2],T)
                    else:
                        current_vote_ts = ""
                if popen:
                    #Voting open
                    bb = ''
                    if current_vote[2]:
                        if current_vote[2] < exptime:
                            bb += "<div class='review' id='%(cand)sreview1' style='padding: 8px;font-weight: bold;background: #C0FFC3;color: black;border: 1px solid black;'>Please review</div><div class='review' id='%(cand)sreview2' style='background:#C0FFC3;padding-top: 4px;'>" % {'cand':c}
                            expvotes += 1
                    for option in ["PP - potential planet", "EB - eclipsing binary", "SV - sinusodial variation", "X - spurious", "O - other non-planet", "BL - blend"]:
                        if option[:2] == str(current_vote[0])[:2]:
                            style = "background:#000088;color:white;border-radius:6px;padding-right:6px;padding-top:3px;padding-bottom:3px"
                            checked = 'checked="checked"'
                        else:
                            style = ""
                            checked=""
                        bb += """<choice name="%(cand)s%(option)s" style="%(style)s">
                        <input type="radio" id="%(cand)s%(option)s" name="%(cand)s" value="%(option)s" %(checked)s onclick="submitting = true;$('#%(cand)sstatus').load('https://%(host)s/%(app)s/default/submit_vote_quick',{'f':'%(field)s','q':'%(cand)s','a':'%(option)s','c':$('#%(cand)scomment').val()})" /><label for="%(cand)s%(option)s">%(option)s</label></choice><br/>""" % {'field':request.args[0],'cand':c,'option':option,'style':style,'checked':checked,'host':request.env.http_host,'app':request.application}
                    bb += """<br/><div id="%(cand)sstatus">%(timestamp)s</div>
                    <textarea 
                    name="%(cand)scomment" 
                    id="%(cand)scomment" 
                    rows="4" 
                    style="font-family: arial, helvetica, sans-serif; font-size:small;"
                    onfocus="if(this.value=='Add a comment...'){this.value=''};$('#%(cand)sstatus').html('Press enter to submit.');"
                    onkeydown="if(event.keyCode==13){submitting = true;this.blur();$('#%(cand)sstatus').load('https://%(host)s/%(app)s/default/submit_vote_quick',{'f':'%(field)s','q':'%(cand)s','a':$('input[name=%(cand)s]:checked').val(),'c':this.value})}"
                    autocomplete="off"
                    >%(comment)s</textarea></td>""" % {'field':request.args[0],'cand':c,'comment':current_vote[1] or "Add a comment...",'host':request.env.http_host,'timestamp':current_vote_ts,'app':request.application}
                    if current_vote[2]:
                        if current_vote[2] < exptime:
                            bb += "</div>"
                elif widget_from_cache:
                    pass #bb already has widget
                else:
                    #Voting closed - tabulate votes now
                    cand_votes = []
                    for vote in votes:
                        if vote.question==c and vote.time > exptime and vote.voter != 1:
                            cand_votes.append(vote)
                    
                    #all_voters = db(db.auth_user.id>0).select()
                    #eligible_voters = []
                    #for i in range(len(all_voters)):
                    #    if db(db.votes.voter==i)(db.votes.time > datetime(datetime.now().year-3,1,1)).count() > 0:
                    #        eligible_voters.append(i)
                    pp = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="PP"]))
                    eb = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="EB"]))
                    sv = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="SV"]))
                    x = str(len([True for vote in cand_votes if str(vote.answer)[:1]=="X"]))
                    o = str(len([True for vote in cand_votes if str(vote.answer)[:1]=="O"]))
                    bl = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="BL"]))
                    #Record to ASCII file
                    ascii_results.append("%s %s %s %s %s %s %s" % (c,pp,eb,sv,x,o,bl))
                    #youvoted = [vote.answer for vote in cand_votes if vote.voter == auth.user_id]
                    #Get all comments
                    comments = []
                    last_date = "00/00"
                    for vote in votes:
                        if vote.question==c and vote.comment != "" and vote.voter:
                            if last_date == datetime.strftime(vote.time,"%b %Y"):
                                comments.append("<b>"+vote.voter.username +":</b> "+ vote.comment)
                            else:
                                comments.append("<br/><span style='color:#4f7ab8;font-style:italic'>" + datetime.strftime(vote.time,"%b %Y") + "</span><br/>"+"<b>"+vote.voter.username +":</b> "+ vote.comment)
                                last_date = datetime.strftime(vote.time,"%b %Y")
                    bb = ''
                    bb += '<table style="font-size:small;">'
                    bb += '<tr style="font-weight:bold;"><td width="16%">PP</td><td width="16%">EB</td><td width="16%">SV</td><td width="16%">X</td><td width="16%">O</td><td width="16%">BL</td></tr>'
                    bb += '<tr><td>' + pp + '</td><td>' + eb + '</td><td>' + sv + '</td><td>' + x + '</td><td>' + o + '</td><td>' + bl + '</td></tr>'
                    bb += '</table>'
                    bb += '<br/><span style="font-size:10pt">'
                    #for y in youvoted:
                    #    if y:
                    #        bb += "<b>You voted:</b> "+y+"<br/><br/>"
                    bb += "##YOUVOTED##<br/>"
                    for comment in comments:
                        bb += comment + "<br/>"
                    bb +="</span>"
                    #bb += "%s eligible voters" % eligible_voters
                    with open('/hd1/'+request.application+'-cache/widgets/'+request.args[0]+'-'+c,'w') as f:
                        f.write(bb)
                #Substitute next available widget placeholder with the widget
                page = re.sub('##WIDGET##',bb,page,1)
            #Display warning for expired votes
            if expvotes > 0:
                if "index" in fname and expvotes > 1:
                    cachetext += "<br/><br/><div style='border: 1px black solid;font-family: \"Trebuchet MS\", Helvetica, sans-serif;background: #C0FFC3;padding: 16px;border-radius: 8px;'><b>Note:</b> You have %s old votes that are set to expire at the end of the voting period, highlighted in green. Please reaffirm or change these votes. <a href='#' onclick='$(\"tbody tr\").hide();$(\".review\").parent().parent().show()'>Review old votes</a> - <a href='https://keltsurvey.org/vote-expiration-kelt-pages' target='_blank'>More information</a></div>" % expvotes
                elif "index" in fname and expvotes == 1:
                    cachetext += "<br/><br/><div style='border: 1px black solid;font-family: \"Trebuchet MS\", Helvetica, sans-serif;background: #C0FFC3;padding: 16px;border-radius: 8px;'><b>Note:</b> You have one old vote that is set to expire at the end of the voting period, highlighted in green. Please reaffirm or change this vote. <a href='#' onclick='$(\"tbody tr\").hide();$(\".review\").parent().parent().show()'>Review the old vote</a> - <a href='https://keltsurvey.org/vote-expiration-kelt-pages' target='_blank'>More information</a></div>"
                else:
                    cachetext += "<br/><br/><div style='border: 1px black solid;font-family: \"Trebuchet MS\", Helvetica, sans-serif;background: #C0FFC3;padding: 16px;border-radius: 8px;'><b>Note:</b> This vote is set to expire at the end of the voting period. Please reaffirm or change this vote. <a href='https://keltsurvey.org/vote-expiration-kelt-pages' target='_blank'>More information</a></div>"
            #Save to cache and write ASCII file
            if (not popen) and ("index" in fname):
                with open(cachename,'w') as f:
                    f.write(page)
                with open(asciiname,'w') as f:
                    f.write("\n".join(ascii_results))
                   
    #http://en.wikipedia.org/wiki/List_of_HTTP_header_fields
    #Force browser to reload each time the user clicks the back button or reopens a closed tab
    response.headers["Expires"] = "Thu, 01 Dec 1994 16:00:00 GMT"
    #Add user info bar
    if use_fname2 and auth.has_membership("admin"):
        cachetext += "<br/><br/><div style='background: white;text-align: left;padding: 4px;border-bottom: 1px #B0B0B0 solid;font-family: \"Trebuchet MS\", Helvetica, sans-serif;'>This page has been loaded from an old directory.</div>"
    if 'popen' in locals():
        response.refresh_enabled = popen
    else:
        response.refresh_enabled = False
    page = page.replace("##USERINFO##",response.render('default/user_info.html')+cachetext)
    #page = page.replace("##YOUVOTED##","")
    #Add 'You voted' indicators
    if 'c' in locals():
        #return repr(c)
        if "index" in fname:
            #Retrieve all votes you cast for a particular field
            print "DB Select: Retrieve all votes you cast for a particular field"
            yourvotes = db(db.votes.question.startswith("K"+c[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True')(db.votes.voter==auth.user_id).select()
        else:
            #Retrieve just your vote for a candidate
            print "DB Select: Retrieve just your vote for a candidate"
            yourvotes = db(db.votes.question.startswith(c[0]))(db.votes.telescope==request.args[0])(db.votes.active=='True')(db.votes.voter==auth.user_id).select()
        #Apply text                  
        for c in candidates:
            candresult = yourvotes.exclude(lambda vote: vote.question.startswith(c) and vote.active==True)
            if len(candresult) > 0 and candresult[0].time < exptime:
                votetext = "Your vote for %s has expired." % (candresult[0].answer,)
            elif len(candresult) > 0:
                votetext = "You voted for %s." % (candresult[0].answer,)
            else:
                votetext = "You did not vote on this candidate."
            #cand_naming_dict = {"north":"C","south":"S","joint":"J"}
            #if auth.has_membership('admin'):
            #    votetext += "<br/>(K%s%s)" % (cand_naming_dict[request.args[0]],c[1:])
            page = page.replace("##YOUVOTED##",votetext,1)
    #If tabulating votes, go to cache page
    if request.vars.tabulate:
        redirect(request.url.split("?")[0])
    #Return the page (no views needed)
    return page
    
#Refreshes the tab with all the votes most recently cast by the user.
def refresh_votes():
    #Is user blocked?
    if auth.has_membership('blocked'):
        return "<script type='text/javascript'>alert('Your account has been deactivated.')</script>"
    #Retrieve votes most recently cast
    print "DB Select: Retrieve votes most recently cast"
    newvotes = db(db.votes.active == "True")(db.votes.voter == auth.user_id)(db.votes.time > datetime.strptime(request.vars.time,'%Y-%m-%d %H:%M:%S.%f')).select()
    #The view sets all the votes using Javascript
    return dict(newvotes=newvotes)
