import md5, random

#create an election
#requires admin access
@auth.requires_membership('admin')
def elections_create():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #generate form using CRUD
    form=crud.create(db.elections,next='elections_read/[id]')
    #return form
    return dict(form=form)

#create an expiry time
#requires admin access
@auth.requires_membership('admin')
def expiry_times_create():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #generate form using CRUD
    form=crud.create(db.expiry_times,next='elections_select')
    #return form
    return dict(form=form)
    
#create a user
#requires admin access
@auth.requires_membership('admin')
def users_create():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #create form using SQLFORM
    form=SQLFORM(db.auth_user, fields=['first_name','last_name','username','email'])
    #add checkbox to create admin
    my_extra_element = TR(LABEL('Admin:'), \
                      INPUT(_name='admin',value=False,_type='checkbox'))
    form[0].insert(-1,my_extra_element)
    #add checkbox to create nonvoting user
    my_extra_element = TR(LABEL('Non-voting:'), \
                      INPUT(_name='novote',value=False,_type='checkbox'))
    form[0].insert(-1,my_extra_element)
    #generate password: 3 letters + 3 numbers
    password = random.choice("abcdefghkmnpqrstuvwxyz") + random.choice("abcdefghkmnpqrstuvwxyz") + random.choice("abcdefghkmnpqrstuvwxyz") + random.choice("2345689") + random.choice("2345689") + random.choice("2345689")
    #form.vars.password = md5.new(password).hexdigest()
    #if form accepts then user is created, but we still need to add
    #admin/novote membership and password
    if form.accepts(request,session):
       #add admin access
       if request.vars.admin:
           auth.add_membership(auth.id_group(role="admin"), form.vars.id)
       #mark user as nonvoting
       if request.vars.novote:
           auth.add_membership(auth.id_group(role="no-vote"), form.vars.id)
       db(db.auth_user.id==form.vars.id).validate_and_update(password=password)
       #return username and password
       return dict(username=form.vars.username,password=password)
    else:
       #return form
       response.flash = 'please fill out the form'
    return dict(form=form)

#read election
#requires admin access
@auth.requires_membership('admin')
def elections_read():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #obtain record
    record = db.elections(request.args(0)) or redirect(URL('error'))
    form=crud.read(db.elections,record)
    #return record
    return dict(form=form)

#read expiry time
#requires admin access
@auth.requires_membership('admin')
def expiry_times_read():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #obtain record
    record = db.expiry_times(request.args(0)) or redirect(URL('error'))
    form=crud.read(db.expiry_times,record)
    #return record
    return dict(form=form)

#update election
#requires admin access
@auth.requires_membership('admin')
def elections_update():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #obtain record
    #if submitted, go to read election page
    #if deleted, go to main admin page
    record = db.elections(request.args(0)) or redirect(URL('error'))
    form=crud.update(db.elections,record,next='elections_read/[id]',
                     ondelete=lambda form: redirect(URL('elections_select')),
                     onaccept=crud.archive)
    #return form
    return dict(form=form)

#update expiry time
#requires admin access
@auth.requires_membership('admin')
def expiry_times_update():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #obtain record
    #if submitted, go to read election page
    #if deleted, go to main admin page
    record = db.expiry_times(request.args(0)) or redirect(URL('error'))
    form=crud.update(db.expiry_times,record,next='elections_select',
                     ondelete=lambda form: redirect(URL('elections_select')),
                     onaccept=crud.archive)
    #return form
    return dict(form=form)
    
#update user
#requires admin access
@auth.requires_membership('admin')
def users_update():
    #link back to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #obtain record
    record = db.auth_user(request.args(0)) or redirect(URL('error'))
    form=SQLFORM(db.auth_user, record)
    #obtain admin access of user
    my_extra_element = TR(LABEL('Admin:'), \
                      INPUT(_name='admin',value=auth.has_membership('admin',record.id),_type='checkbox',_disabled=(record.id==auth.user_id)))
    form[0].insert(-1,my_extra_element)
    #obtain non-voting of user
    my_extra_element = TR(LABEL('Non-voting:'), \
                      INPUT(_name='novote',value=auth.has_membership('no-vote',record.id),_type='checkbox'))
    form[0].insert(-1,my_extra_element)
    #obtain blocked status of user
    my_extra_element = TR(LABEL('Blocked:'), \
                      INPUT(_name='blocked',value=auth.has_membership('blocked',record.id),_type='checkbox'))
    form[0].insert(-1,my_extra_element)
    #if accepted
    if form.accepts(request,session):
       #deleting records is done by adding "blocked" to the user.
       #delete_this_record is always false
       if not request.vars.delete_this_record:
           #admin access
           if request.vars.admin:
               auth.add_membership(auth.id_group(role="admin"), form.vars.id)
           else:
               auth.del_membership(auth.id_group(role="admin"), form.vars.id)
           #novote access
           if request.vars.novote:
               auth.add_membership(auth.id_group(role="no-vote"), form.vars.id)
           else:
               auth.del_membership(auth.id_group(role="no-vote"), form.vars.id)
           #blocked
           if request.vars.blocked:
               auth.add_membership(auth.id_group(role="blocked"), form.vars.id)
           else:
               auth.del_membership(auth.id_group(role="blocked"), form.vars.id)
       #return to main admin page
       redirect(URL('elections_select'))
    else:
       #return form
       response.flash = 'please fill out the form'
    return dict(form=form)

#main admin page
#requires admin access
@auth.requires_membership('admin')
def elections_select():
    #add link to main admin page
    response.menu += [
        (T('Admin tools'), False, URL('admintools','elections_select'), [])
    ]
    #get elections
    f,v=request.args(0),request.args(1)
    try: query=f and db.elections[f]==v or db.elections
    except: redirect(URL('error'))
    rows=db(query).select()
    #get expiry data
    exptimes = db(db.expiry_times).select()
    #get user data
    users=db(db.auth_user).select()
    return dict(rows=rows,exptimes=exptimes,users=users)

#delete user
#requires admin access
@auth.requires_membership('admin')
def users_delete():
    #add blocked status to user. Will still be on record but not on user list
    auth.add_membership(auth.id_group(role="blocked"), int(request.args(0)))
    redirect(URL('elections_select'))