# coding: utf8
# try something like
import re, os

#IGNORE - this is old code

@auth.requires_login()
def quick():
    if ".." in request.args:
        return "Nope"
    fname = request.url
    if fname[-1] == "/":
        redirect(fname[:-1])
    fname = fname[fname.index("quick"):]
    debug = False
    if fname.endswith("+"):
        debug = True
        fname = fname[:-1]
    fname_rules = [
        ("re","(\d+)/figs_\\1", "figs_\\1"),
        ("re","^quick/north/(\d+)$", "/blue/stassunlab/kelt/nkelt/cand_selection/web/index_\\1.html"),
        ("re","^quick/north/","/blue/stassunlab/kelt/nkelt/cand_selection/web/"),
        ("re","^quick/south/(\d+)$", "/blue/stassunlab/kelt/nkelt/ks_cand_selection/web/index_\\1.html"),
        ("re","^quick/south/","/blue/stassunlab/kelt/nkelt/ks_cand_selection/web/")
    ]
    for r in fname_rules:
        if r[0] == "re":
            fname = re.sub(r[1],r[2],fname)
        else:
            fname = fname.replace(r[1],r[2])
    if debug:
        return fname
    if fname.endswith("png"):
        #https://en.wikipedia.org/wiki/Mime_type
        #http://web2py.com/examples/default/examples
        response.headers['Content-Type']="image/png"
        response.headers['Content-Disposition']='attachment; filename="'+fname.split('/')[-1]+'"'
        return response.stream(open(fname,'rb'),chunk_size=4096)
    try:
        f = open(fname,'r')
        page = f.read()
        f.close()
    except:
        raise HTTP(404)
    page_rules = [
        ('index','re','<script.*voting_table_v4.js.*>(</script>)?', ""), #remove old js
        ('index','exact','<script type="text/javascript" id="js">','<script type="text/javascript" src="/vote/static/js/jquery.lazyload.min.js"></script><script type="text/javascript" id="js">'), #lazyload
        ('index','exact','img src="figs','img class="lazy" style="width:600px;height:300px" src="/vote/static/images/gray.png" data-original="figs'), #lazyload
        ('index','exact','tablesorter();','tablesorter();$("img.lazy").lazyload({ effect : "fadeIn", threshold:500});'), #lazyload
        ('','re','<body([^>]*)>', '<body\\1><iframe src="/vote/default/user_info/" width="100%" height="32px" scrolling="no" marginwidth="0" marginheight="0" frameBorder="0" style="border:0px;position:fixed;left:0px;right:0px;top:0px"></iframe><div style="height:24px;"></div><br/><br/>'), #userinfo
        ('','re','pagehelp.html','/quick/north/pagehelp.html'), #help
        ('','re','indexhelp.html','"/quick/north/indexhelp.html"'), #help
        ('','re','../index_(0\d).html','/quick/north/\\1'), #index links
        ('','re','../index_(1\d).html','/quick/north/\\1'),
        ('','re','../index_(20).html','/quick/north/\\1'),
        ('','re','../index_(27).html','/quick/south/\\1'),
        ('index','re','index_(0\d).html','/quick/north/\\1'),
        ('index','re','index_(1\d).html','/quick/north/\\1'),
        ('index','re','index_(20).html','/quick/north/\\1'),
        ('index','re','index_(27).html','/quick/south/\\1'),
        ('index','re','<a([^"]*?)>(.*?)</a>','<a\\1>\\2</a>%%\\2%%'), #candidates
        ('ks','exact','KELT-North','KELT-South') #title
    ]
    for r in page_rules:
        if r[0] in fname:
            if r[1] == "re":
                page = re.sub(r[2],r[3],page,re.DOTALL)
            else:
                page = page.replace(r[2],r[3])
      
    if 'index' in fname:
        candidates = [c[2:-2] for c in re.findall("%%.*?%%",page)]
        popen = polls_open(candidates[0])
        for c in candidates:
            if auth.user_id != 1:
                votes = db(db.votes.question.startswith(c))(db.votes.active=='True')(db.votes.voter != 1).select()
            else:
                votes = db(db.votes.question.startswith(c))(db.votes.active=='True').select()
            current_vote = [(vote.answer,vote.comment) for vote in votes if vote.question==c and vote.voter.id==auth.user_id]
            if len(current_vote)>0:
                current_vote = current_vote[0]
            else:
                current_vote = ("","")
            if popen:
                bb = '<td style="vertical-align: middle; background-color: C6DEFF; text-align: left; font-size:12px; width:170px">'
                for option in ["PP - potential planet", "EB - eclipsing binary", "SV - sinusodial variation", "X - spurious", "O - other non-planet", "BL - blend"]:
                    if option[:2] == str(current_vote[0])[:2]:
                        style = "background:#000088;color:white;border-radius:6px;padding-right:6px;padding-top:3px;padding-bottom:3px"
                        checked = 'checked="checked"'
                    else:
                        style = ""
                        checked=""
                    bb += """<choice name="%(cand)s%(option)s" style="%(style)s">
                    <input type="radio" id="%(cand)s%(option)s" name="%(cand)s" value="%(option)s" %(checked)s onclick="$('#%(cand)sstatus').load('https://%(host)s/vote/default/submit_vote_quick',{'q':'%(cand)s','a':'%(option)s','c':$('#%(cand)scomment').val()})" /><label for="%(cand)s%(option)s">%(option)s</label></choice><br/>""" % {'cand':c,'option':option,'style':style,'checked':checked,'host':request.env.http_host}
                bb += """<br/><div id="%(cand)sstatus"></div>
                <textarea 
                name="%(cand)scomment" 
                id="%(cand)scomment" 
                rows="4" 
                style="font-family: arial, helvetica, sans-serif; font-size:small;"
                onfocus="if(this.value=='Add a comment...'){this.value=''};$('#%(cand)sstatus').html('Press enter to submit.');"
                onkeydown="if(event.keyCode==13){this.blur();$('#%(cand)sstatus').load('https://%(host)s/vote/default/submit_vote_quick',{'q':'%(cand)s','a':$('input[name=%(cand)s]:checked').val(),'c':this.value})}"
                autocomplete="off"
                >%(comment)s</textarea></td>""" % {'cand':c,'comment':current_vote[1] or "Add a comment...",'host':request.env.http_host}
            else:
                cand_votes = []
                for vote in votes:
                    if vote.question==c:
                        cand_votes.append(vote)
                pp = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="PP"]))
                eb = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="EB"]))
                sv = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="SV"]))
                x = str(len([True for vote in cand_votes if str(vote.answer)[:1]=="X"]))
                o = str(len([True for vote in cand_votes if str(vote.answer)[:1]=="O"]))
                bl = str(len([True for vote in cand_votes if str(vote.answer)[:2]=="BL"]))
                youvoted = [vote.answer for vote in cand_votes if vote.voter.id == auth.user_id]
                comments = ["<b>"+vote.voter.username +":</b> "+ vote.comment for vote in votes if vote.question==c and vote.comment != ""]
                #comments = db(db.votes.question==cand)(db.votes.active=='True')(db.votes.voter != 1).select(db.votes.comment,db.votes.voter)
                bb = '<td style="vertical-align: middle; background-color: C6DEFF; text-align: left;">'
                bb += '<table style="font-size:small;">'
                bb += '<tr style="font-weight:bold;"><td width="16%">PP</td><td width="16%">EB</td><td width="16%">SV</td><td width="16%">X</td><td width="16%">O</td><td width="16%">BL</td></tr>'
                bb += '<tr><td>' + pp + '</td><td>' + eb + '</td><td>' + sv + '</td><td>' + x + '</td><td>' + o + '</td><td>' + bl + '</td></tr>'
                bb += '</table>'
                bb += '<br/><span style="font-size:10pt">'
                for y in youvoted:
                    if y:
                        bb += "<b>You voted:</b> "+y+"<br/><br/>"
                for comment in comments:
                    bb += comment + "<br/><br/>"
                bb +="</span>"
                bb += '</td>'
            page = re.sub('<td style="vertical-align: middle; background-color: C6DEFF; text-align: center;">            </td>',bb,page,1)
        page = re.sub("%%.*?%%","",page)
    
    return page
