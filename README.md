# The KELT North & South Candidate Selection Page ("KELT Voting")

## ACCOUNTS

The root password for MySQL.

* username: root
* server: localhost
* password: __________________

The password for KELT Voting to access the database.

* username: vote
* server: localhost
* password: __________________
* database: vote

The Twitter account to KELT Voting (http://twitter.com/keltvoting).

* username: keltvoting
* password: __________________

The info.filtergraph@gmail.com account, used to send password retrieval emails.

* username: info.filtergraph@gmail.com
* password: __________________

## HISTORY

The KELT Voting pages were established in June 2011 as a method for evaluating stars in the night sky ("candidates") into categories. The categories are potential planet (PP), eclipsing binary (EB), sinusodial variation (SV), spurious (X), other (O), and blend (BL).

Originally, the candidate pages were served on a static web server that contained voting ballots and results within an iframe widget. As time progressed we ended up with fields containing 200+ candidates resulting in 200+ queries to the web server. Each index page would load in about a minute, causing frustration to many KELT users worldwide.

To remedy this we rewrote the code. Instead of returning iframe widgets, the server loads a static file, makes modifications based on the current state of voting, and returns those modifications to the user. We refer to this as quick loading. To make the index files load faster after voting, the results are cached once voting is finished. We included a "refresh cache" link to override the cache (for instance, if Josh decides to rank the candidates based on the number of users who think a candidate has a potential planet).

Another problem we encountered is if a user votes on one web browser, the changes weren't reflected when the user clicks on the Back button or opens the page in a different web browser (or even a different computer). To remedy this we removed caching from the web server in those cases and added Javascript code to synchronize the data.

## PAGE STRUCTURE

As of this writing there are two telescopes in the KELT system:

* KELT North, located in Arizona and operated by Ohio State
* KELT South, located in South Africa and operated by Vanderbilt

Each of them cover a number of fields in the night sky:

* 14 fields in KELT North 
* 16 fields in KELT South
* 1 joint field (contains candidates covered by KELT North and KELT South)

The fields are all distinct from each other, even if the field numbers are identical. For instance, KELT North field 12 is completely different from KELT South field 12.

Each field number is two digits. When the field number is less than 10 the leading zero is added.

Each field contains many candidates, ranging from 104 in KELT North field 20 to 1233 in KELT North field 04. Candidates are named by this convention:

* the telescope (KC for KELT North, KS for KELT South, KJ for joint fields)
* the two digit field number
* "C"
* the six digit candidate number

It is possible for two stars in different telescopes to have identical field and candidate numbers. For instance, KS12C012345 and KC12C012345 would be completely different and should be handled separately.

There are many different pages within the KELT voting system:

* The list of fields. Lets people know when the fields are open for voting and for how long.
* The index pages. Contain basic information about each candidate in the field. If the field is open, display a ballot for each candidate; if not, display the results.
* The candidate pages. Contain more detailed information about each candidate. Ballots and results are available here as well.
* Each candidate may contain one or more of the following:
    * Variablilty pages (contain ballots and results)
    * SuperWASP pages (contain ballots and results)
    * ASAS pages (contain ballots and results)
    * RFP plots (those are handled on a separate web server and do not contain ballots or results)
* A page to change user info
* A page to review votes (voting users only)
* An admin section (admin users only) for managing elections and handling users.

## ADMINISTRATIVE STRUCTURE

There are three groups of users:

* admin: can add, modify and delete users and elections. There are five admins: me, Josh, Rob, Keivan, and Joey.
* non-voting: can view the candidates as well as results as they come in, but they can't vote.
* blocked: effectively deleted from the system. If the user logs in, a message will appear saying that he/she is blocked from the system; the user can only edit user info and logout.

As user number 1, I can cast votes for the sake of testing; however, these votes are not tabulated in the final results.

## DIRECTORY STRUCTURE

All files for KELT Voting are in /hd1. Originally they were stored in /home/burgerdm (which is connected to the BlueArc network) but then we moved all the files to /hd1 to improve server reliability and performance.

* hd1
    * web2py
        * routes.py: redirect rules. All old Filtergraph traffic gets redirected to filtergraph.com, and all KELT Voting traffic gets redirected to their proper places. For instance, "/kelt/north/10" becomes "/vote/default/quick/north/10".
	    * applications
	        * vote
	            * models
		            * db.py: database
		            * functions.py: contains a function that returns true or false depending on whether a particular poll is open (if true, returns the closing time)
		        * controllers
		            * admintools.py: controls poll opening/closing, adding and deleting users
		            * appadmin.py: controls database
		            * default.py: controls normal use of the site
		        * views
		            * admintools
		                * elections_create.html: creating an election
			            * elections_read.html: read details about an election
			            * elections_select.html: read all elections and users
			            * elections_update.html: edit or delete an election
			            * users_create.html: create a user
			            * users_update.html: edit a user
		            * default
		                * refresh_votes.html: contains Javascript code for refreshing vote selection between browsers
			            * review_votes.html: contains 
			            * user.html: pages for user to edit profile information, change passwords, etc.
		        * static
		            * images
		                * kelt voting 64px.png: the KELT Voting logo
		

There are a number of other files in the repository but they should have no effect in program operation. I left them in the program just in case.

## WEB FRAMEWORK

We are using Web2py version 2.5.1 to serve the KELT pages. Unfortunately, upgrading to a later version would require modifications to the database and code; specifically, the later versions ban column and table names that are reserved keywords such as "time" and "minimum".
	
## DATABASE

The database for KELT Voting is on MySQL.

The root account allows access to the entire MySQL system, while the vote account only allows access to the "vote" database.

The easiest way to access the database is to run web2py:

    cd /hd1/web2py
    python web2py.py -a "<recycle>"

Then go to http://127.0.0.1:8000/vote/appadmin/ to modify database records. Here you can also make CSV backups of each table in the database.

### VOTES TABLE

When the user casts a vote, it is recorded in the database. When the user changes the vote or leaves a comment, the old vote remains on the record but it is changed to be inactive.

* question: the name of the candidate. For consistency among previous versions of the site, the letter designating the telescope name is stripped. For instance, KC12C012345 becomes K12C012345.
* answer: the result of the vote for that candidate as a string. Can be one of the following: "PP - potential planet", "EB - eclipsing binary", "SV - sinusodial variation", "X - spurious", "O - other", or "BL - blend".
* comment: the comment left by the user
* active: whether or not the vote is active (see above)
* voter: the user who cast the vote
* time: the time of the vote
* telescope: the name of the telescope; either "north", "south" or "joint"

### FEEDBACK TABLE

This has no effect in the system but I left it in just in case.

### ELECTIONS TABLE

* name: the name of the election. Typically this contains the telescope name and field name; for instance, "south-K20" opens KELT South field 20. There are also records such as "K20" that open field 20 for all telescopes, but this is no longer typically used.
* open_time: the time the election is open.
* close_time: the time the election is closed. To close the election manually, adjust this to a time before the present.